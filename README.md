# tall.bot
[![builds.sr.ht status](https://builds.sr.ht/~armaan/tall.bot/commits/.build.yml.svg)](https://builds.sr.ht/~armaan/tall.bot/commits/.build.yml?)

A simple discord Wikipedia bot

## Installation
Either run the docker container `armaanb/tall.bot` with the TOKEN environment variable set, or just run the script with the token as the only argument

## License
Copyright Armaan Bhojwani 2021, MIT license
